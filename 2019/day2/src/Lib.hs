module Lib where

import Debug.Trace

origIntCodes = [1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,1,9,19,1,13,19,23,2,23,9,27,1,6,27,31,2,10,31,35,1,6,35,39,2,9,39,43,1,5,43,47,2,47,13,51,2,51,10,55,1,55,5,59,1,59,9,63,1,63,9,67,2,6,67,71,1,5,71,75,1,75,6,79,1,6,79,83,1,83,9,87,2,87,10,91,2,91,10,95,1,95,5,99,1,99,13,103,2,103,9,107,1,6,107,111,1,111,5,115,1,115,2,119,1,5,119,0,99,2,0,14,0]

-- used for final result
intCodes = [1,12,2,3,1,1,2,3,1,3,4,3,1,5,0,3,2,1,9,19,1,13,19,23,2,23,9,27,1,6,27,31,2,10,31,35,1,6,35,39,2,9,39,43,1,5,43,47,2,47,13,51,2,51,10,55,1,55,5,59,1,59,9,63,1,63,9,67,2,6,67,71,1,5,71,75,1,75,6,79,1,6,79,83,1,83,9,87,2,87,10,91,2,91,10,95,1,95,5,99,1,99,13,103,2,103,9,107,1,6,107,111,1,111,5,115,1,115,2,119,1,5,119,0,99,2,0,14,0]

replace pos newVal list = trace ("pos to replace: " ++ show pos ++ " with val: " ++ show newVal) take pos list ++ newVal : drop (pos+1) list

-- blows up still, but worked with the intCodes input
-- *Main Lib> r
-- [1,1,1,4,99,5,6,0,99]
-- *Main Lib> processOp 0 r
-- [1,1,1,4,99,5,6,0,99] opCode: 1 startingIdx: 0 idxToReplace: 4
-- pos to replace: 4 with val: 2
-- [1,1,1,4,2,5,6,0,99] opCode: 2 startingIdx: 4 idxToReplace: 0
-- pos to replace: 0 with val: 30
-- *** Exception: Prelude.!!: index too large

processOp startingIdx codes = do
  let opCode = trace (show codes ++ " opCode: " ++ show (codes !! startingIdx) ++ " startingIdx: " ++ show startingIdx ++ " idxToReplace: " ++ show idxToReplace) codes !! startingIdx

  case opCode of
    1 -> processOp (startingIdx + 4) (replace idxToReplace (val1 + val2) codes)
    2 -> processOp (startingIdx + 4) (replace idxToReplace (val1 * val2) codes)
    _ -> codes

  where val1Idx      = codes !! (startingIdx + 1)
        val2Idx      = codes !! (startingIdx + 2)
        idxToReplace = codes !! (startingIdx + 3)
        val1         = codes !! val1Idx
        val2         = codes !! val2Idx

someFunc :: [Int]
someFunc = processOp 0 intCodes
