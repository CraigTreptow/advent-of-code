module Lib where

startingRange = [109165 .. 576723]

digits :: Integral x => x -> [x]
digits 0 = []
digits x = digits (x `div` 10) ++ [x `mod` 10]

digitsIncreasing :: [Integer] -> Bool
digitsIncreasing [] = True
digitsIncreasing [x] = True
digitsIncreasing (x:y:xs) = x <= y && digitsIncreasing (y:xs)

hasDouble :: [Integer] -> Bool
hasDouble [] = False
hasDouble [x] = False
hasDouble (x:y:xs) = x == y || hasDouble (y:xs)

hasTriple :: [Integer] -> Bool
hasTriple [] = False
hasTriple [x] = False
hasTriple [x,y] = False
hasTriple (x:y:z:xs) = (x == y && x == z) || hasTriple (z:xs)

hasDoubleNotTriple :: [Integer] -> Bool
hasDoubleNotTriple xs =
  (hasDouble xs) && not (hasTriple xs)

partOne =
  [ x | x <- startingRange, digitsIncreasing (digits x), hasDouble (digits x)]

partTwo =
  -- [ x | x <- startingRange, digitsIncreasing (digits x), hasDouble (digits x), not (hasTriple (digits x))]
  [ x | x <- startingRange, digitsIncreasing (digits x), hasDoubleNotTriple (digits x)]

someFunc :: IO ()
someFunc = putStrLn ("Part One: " ++ (show (length partOne)) ++ "\n" ++ "Part Two: " ++ (show (length partTwo)))
